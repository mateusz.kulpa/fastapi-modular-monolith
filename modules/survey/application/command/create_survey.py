import dataclasses


@dataclasses.dataclass
class CreateSurveyCommandPayload:
    type: str


class CreateSurveyCommand:
    def __init__(self):
        ...

    def execute(self, payload: CreateSurveyCommandPayload):
        print('executing CreateSurveyCommand...')
        print(payload)
