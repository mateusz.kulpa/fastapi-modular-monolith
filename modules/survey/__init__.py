from dependency_injector import containers, providers
from fastapi import FastAPI

from core.abstractions.module import Module
from core.abstractions.module_client import ModuleClient
from modules.survey.api.routes.survey import router as survey_router
from modules.survey.application.command.create_survey import CreateSurveyCommand, CreateSurveyCommandPayload


class SurveyModule(Module):
    @staticmethod
    def register(app: FastAPI):
        app.include_router(survey_router)

    @staticmethod
    def register_dependencies(container: containers.DynamicContainer):
        container.set_provider(CreateSurveyCommand.__name__, providers.Factory(CreateSurveyCommand))

        SurveyModule._subscribe_module_client(container)

    @staticmethod
    def _subscribe_module_client(container: containers.DynamicContainer):
        module_client: ModuleClient = container.providers.get(ModuleClient.__name__)()

        create_survey_command: CreateSurveyCommand = container.providers.get(CreateSurveyCommand.__name__)()
        module_client.subscribe(
            "survey/create",
            lambda payload: create_survey_command.execute(CreateSurveyCommandPayload(**payload))
        )
