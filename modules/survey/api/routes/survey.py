from fastapi import APIRouter
router = APIRouter(prefix="/survey", tags=["survey"])


@router.get("/{id}")
async def get(id: str):
    return {"id": id}
