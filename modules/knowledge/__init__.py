from dependency_injector import containers
from fastapi import FastAPI, APIRouter
from core.abstractions.module import Module


class KnowledgeModule(Module):
    @staticmethod
    def register(app: FastAPI):
        app.include_router(APIRouter(prefix="/knowledge", tags=["knowledge"]))

    @staticmethod
    def register_dependencies(container: containers.DynamicContainer):
        ...