from dependency_injector import containers
from fastapi import FastAPI, APIRouter
from core.abstractions.module import Module


class UserProfileModule(Module):
    @staticmethod
    def register(app: FastAPI):
        app.include_router(APIRouter(prefix="/user_profile", tags=["user_profile"]))

    @staticmethod
    def register_dependencies(container: containers.DynamicContainer):
        ...