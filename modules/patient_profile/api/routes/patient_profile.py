from dependency_injector.wiring import inject, Provide
from fastapi import APIRouter, Depends
from pydantic import BaseModel

from modules.patient_profile.application.command.create_patient_profile import CreatePatientProfileCommand
from modules.patient_profile.application.query.get_patient_profile_by_id import GetPatientProfileByIdQuery

router = APIRouter(prefix="/patient_profile", tags=["patient_profile"])


@router.get("/{id}")
@inject
async def get(
    id: str,
    get_patient_profile_by_id_query: GetPatientProfileByIdQuery = Depends(Provide[GetPatientProfileByIdQuery.__name__]),
):
    return get_patient_profile_by_id_query.query(id)


class CreatePatientProfileAge(BaseModel):
    value: int
    unit: str


class CreatePatientProfile(BaseModel):
    id: str
    name: str
    age: CreatePatientProfileAge


@router.post("/")
@inject
async def create(
    data: CreatePatientProfile,
    create_patient_profile_command: CreatePatientProfileCommand = Depends(Provide[CreatePatientProfileCommand.__name__])
):
    create_patient_profile_command.execute(
        patient_profile_id=data.id,
        name=data.name,
        age_value=data.age.value,
        age_unit=data.age.unit,
        evidences=[],
    )
