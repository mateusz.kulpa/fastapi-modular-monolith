from dependency_injector import containers, providers
from fastapi import FastAPI
from core.abstractions.events import EventsRegistry
from core.abstractions.module import Module
from modules.patient_profile.api.routes.patient_profile import router as patient_profile_router
from modules.patient_profile.application.command.create_patient_profile import CreatePatientProfileCommand
from modules.patient_profile.application.query.get_patient_profile_by_id import GetPatientProfileByIdQuery
from modules.patient_profile.domain.repositories import PatientProfileRepository
from modules.patient_profile.infrastructure.event_handlers import PatientCreatedHandler, PatientCreated
from modules.patient_profile.infrastructure.repositories import PatientProfileRepositoryImpl


class PatientProfileModule(Module):
    @staticmethod
    def register(app: FastAPI):
        app.include_router(patient_profile_router)

    @staticmethod
    def register_dependencies(container: containers.DynamicContainer):
        container.set_provider(PatientProfileRepository.__name__, providers.Factory(PatientProfileRepositoryImpl))
        container.set_provider(GetPatientProfileByIdQuery.__name__, providers.Factory(GetPatientProfileByIdQuery))
        container.set_provider(CreatePatientProfileCommand.__name__, providers.Factory(CreatePatientProfileCommand))
        PatientProfileModule._register_event_handlers(container)

    @staticmethod
    def _register_event_handlers(container: containers.DynamicContainer):
        events_registry: EventsRegistry = container.providers.get(EventsRegistry.__name__)()
        events_registry.register(PatientCreated, PatientCreatedHandler)
