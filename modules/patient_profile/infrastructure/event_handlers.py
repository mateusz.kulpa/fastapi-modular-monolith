from dependency_injector.wiring import inject, Provide

from core.abstractions.database import Database
from core.abstractions.email_sender import EmailSender
from core.abstractions.events import EventHandler
from modules.patient_profile.application.event.patient_created import PatientCreated


class PatientCreatedHandler(EventHandler):
    @inject
    def __init__(self, email_sender: EmailSender = Provide[EmailSender.__name__]):
        self.email_sender = email_sender

    def handle(self, event: PatientCreated):
        print(f'handling "patient created" event: {event}')
        self.email_sender.send(
            to="user@example.com",
            content="Your account has been created!"
        )
