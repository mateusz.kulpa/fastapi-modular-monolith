from dataclasses import asdict
from dependency_injector.wiring import inject, Provide
from core.abstractions.database import Database
from modules.patient_profile.domain.entities import PatientProfile
from modules.patient_profile.domain.value_objects import PatientProfileId
from modules.patient_profile.infrastructure.models import PatientProfileModel


class PatientProfileRepositoryImpl:
    @inject
    def __init__(self, db: Database = Provide[Database.__name__]) -> None:
        self.db = db
        self.model = PatientProfileModel

    def create(self, patient_profile: PatientProfile):
        with self.db.session() as session:
            session.add(PatientProfileModel(
                id=patient_profile.id.value,
                name=patient_profile.name,
                age=asdict(patient_profile.age),
                evidences=patient_profile.evidences,
            ))
            session.commit()

    def update(self, patient_profile: PatientProfile):
        ...

    def get_by_id(self, patient_profile_id: PatientProfileId) -> PatientProfile:
        with self.db.session() as session:
            return session.query(
                self.model
            ).filter(
                self.model.id == patient_profile_id.value
            ).first()
