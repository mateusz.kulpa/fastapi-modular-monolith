from sqlalchemy import Column, String
from sqlalchemy.dialects.sqlite import JSON
from core.infractucture.database import Base



class PatientProfileModel(Base):
    __tablename__ = "patient_profile.patient_profile"

    id = Column(String, primary_key=True, index=True)
    name = Column(String)
    age = Column(JSON())
    evidences = Column(JSON())

