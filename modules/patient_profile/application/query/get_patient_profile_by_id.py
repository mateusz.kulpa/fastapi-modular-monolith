from dependency_injector.wiring import inject, Provide

from modules.patient_profile.domain.entities import PatientProfile
from modules.patient_profile.domain.repositories import PatientProfileRepository
from modules.patient_profile.domain.value_objects import PatientProfileId


class GetPatientProfileByIdQuery:
    @inject
    def __init__(
        self,
        patient_profile_repository: PatientProfileRepository = Provide[PatientProfileRepository.__name__]
    ):
        self.patient_profile_repository = patient_profile_repository

    def query(self, patient_profile_id: str) -> PatientProfile:
        return self.patient_profile_repository.get_by_id(PatientProfileId(patient_profile_id))


