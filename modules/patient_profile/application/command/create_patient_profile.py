from dependency_injector.wiring import Provide, inject

from core.abstractions.events import EventDispatcher
from core.abstractions.module_client import ModuleClient
from modules.patient_profile.application.event.patient_created import PatientCreated
from modules.patient_profile.domain.entities import PatientProfile
from modules.patient_profile.domain.repositories import PatientProfileRepository
from modules.patient_profile.domain.value_objects import PatientProfileId, PatientAge, Evidence


class CreatePatientProfileCommand:
    @inject
    def __init__(
        self,
        patient_profile_repository: PatientProfileRepository = Provide[PatientProfileRepository.__name__],
        event_dispatcher: EventDispatcher = Provide[EventDispatcher.__name__],
        module_client: ModuleClient = Provide[ModuleClient.__name__]
    ):
        self.patient_profile_repository = patient_profile_repository
        self.event_dispatcher = event_dispatcher
        self.module_client = module_client

    def execute(self, patient_profile_id: str, name: str, age_value: int, age_unit: str, evidences: list[(str, str)]):
        patient_profile = PatientProfile(
            id=PatientProfileId(value=patient_profile_id),
            name=name,
            age=PatientAge(
                value=age_value,
                unit=age_unit,  # type: ignore
            ),
            evidences=list(map(lambda ev: Evidence(id=ev[0], choice_id=ev[1]), evidences))
        )

        self.patient_profile_repository.create(patient_profile)
        self.module_client.send('survey/create', {"type": "test"})
        self.event_dispatcher.handle(PatientCreated(
            id=patient_profile_id,
            name=name
        ))

