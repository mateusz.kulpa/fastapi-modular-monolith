import dataclasses
from core.abstractions.events import Event


@dataclasses.dataclass
class PatientCreated(Event):
    id: str
    name: str
