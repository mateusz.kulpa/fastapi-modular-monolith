from typing import Protocol

from modules.patient_profile.domain.entities import PatientProfile
from modules.patient_profile.domain.value_objects import PatientProfileId


class PatientProfileRepository(Protocol):
    def create(self, patient_profile: PatientProfile):
        ...

    def update(self, patient_profile: PatientProfile):
        ...

    def get_by_id(self, patient_profile_id: PatientProfileId) -> PatientProfile:
        ...
