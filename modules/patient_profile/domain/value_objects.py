from dataclasses import dataclass
from typing import Literal


@dataclass(frozen=True)
class PatientProfileId:
    value: str


@dataclass(frozen=True)
class PatientAge:
    value: int
    unit: Literal["year", "month"]


@dataclass(frozen=True)
class Evidence:
    id: str
    choice_id: str
