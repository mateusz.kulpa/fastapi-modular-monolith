from dataclasses import dataclass

from modules.patient_profile.domain.value_objects import PatientAge, PatientProfileId, Evidence


@dataclass(kw_only=True)
class PatientProfile:
    id: PatientProfileId
    name: str
    age: PatientAge
    evidences: list[Evidence]

    def get_all_present_evidences(self) -> list[Evidence]:
        return list(filter(lambda evidence: evidence.answer == "present", self.evidences))

    def is_adult(self) -> bool:
        if self.age.unit == "year" and self.age.value >= 18:
            return True
        if self.age.unit == "month" and self.age.value // 12 >= 18:
            return True
        return False
