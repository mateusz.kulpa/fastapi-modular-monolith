from typing import Any, Callable
from core.abstractions.module_client import ModuleClient, TResult


class MemoryModuleClient(ModuleClient):
    registered_actions: dict[str, Callable] = dict()

    def subscribe(self, path: str, action: Callable[[dict], TResult]) -> None:
        if path in self.registered_actions:
            raise Exception(f"Action already registered with path: {path}")

        self.registered_actions[path] = action

    def send(self, path: str, payload: dict) -> TResult:
        if path not in self.registered_actions:
            raise Exception(f"No action has been defined for path: '{path}'.")

        return self.registered_actions[path](payload)

    def publish(self, path: str, payload: dict):
        self.registered_actions[path](payload)

