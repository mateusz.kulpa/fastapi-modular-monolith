from typing import Type

from dependency_injector.wiring import Provide, inject

from core.abstractions.events import EventDispatcher, Event, EventsRegistry, EventHandler


class MemoryEventsRegistry(EventsRegistry):
    event_handlers: dict[str, list[Type[EventHandler]]] = dict()

    def register(self, event: Type[Event], event_handler: Type[EventHandler]):
        event_identifier = event.__name__
        if event_identifier not in self.event_handlers:
            self.event_handlers[event_identifier] = []

        self.event_handlers[event_identifier].append(event_handler)

    def get_handlers(self, event: Type[Event]) -> list[Type[EventHandler]]:
        event_identifier = event.__name__
        if event_identifier not in self.event_handlers:
            return []

        return self.event_handlers[event_identifier]


class MemoryEventDispatcher(EventDispatcher):
    @inject
    def __init__(
        self,
        events_registry: EventsRegistry = Provide[EventsRegistry.__name__]
    ):
        self.events_registry = events_registry

    def handle(self, event: Event):
        handlers = self.events_registry.get_handlers(type(event))
        for handler in handlers:
            handler_instance = handler()
            handler_instance.handle(event)
