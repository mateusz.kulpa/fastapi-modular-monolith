from dependency_injector import containers, providers
from core.abstractions.database import Database
from core.abstractions.email_sender import EmailSender
from core.abstractions.events import EventDispatcher, EventsRegistry
from core.abstractions.module_client import ModuleClient
from core.infractucture.database import DatabaseImpl
from core.infractucture.email_sender import SMTPEmailSender
from core.infractucture.events import MemoryEventDispatcher, MemoryEventsRegistry
from core.infractucture.module_client import MemoryModuleClient

application_container = containers.DynamicContainer()
application_container.set_provider(Database.__name__,  providers.Factory(DatabaseImpl))
application_container.set_provider(ModuleClient.__name__, providers.Singleton(MemoryModuleClient))
application_container.set_provider(EventsRegistry.__name__, providers.Singleton(MemoryEventsRegistry))
application_container.set_provider(EventDispatcher.__name__, providers.Factory(MemoryEventDispatcher))
application_container.set_provider(EmailSender.__name__, providers.Factory(SMTPEmailSender))
