from core.abstractions.email_sender import EmailSender


class SMTPEmailSender(EmailSender):
    def send(self, to: str, content: str):
        print(f'sending email to: {to}')
        print(content)
