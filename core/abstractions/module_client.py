from abc import ABC, abstractmethod
from typing import Any, TypeVar, Callable

TResult = TypeVar('TResult')


class ModuleClient(ABC):
    @abstractmethod
    def subscribe(self, path: str, action: Callable[[dict], TResult]) -> None:
        ...

    @abstractmethod
    def send(self, path: str, payload: dict) -> TResult:
        ...

    @abstractmethod
    def publish(self, path: str, payload: dict):
        ...
