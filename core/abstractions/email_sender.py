from abc import ABC


class EmailSender(ABC):
    def send(self, to: str, content: str):
        ...
