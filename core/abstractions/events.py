from abc import ABC, abstractmethod
from typing import Type, TypeVar, Generic


class Event(ABC):
    ...


TEvent = TypeVar('TEvent', bound='Event')


class EventHandler(ABC, Generic[TEvent]):
    @abstractmethod
    def handle(self, event: TEvent):
        ...


class EventDispatcher(ABC):
    @abstractmethod
    def handle(self, event: Event):
        ...


class EventsRegistry(ABC):
    @abstractmethod
    def register(self, event: Type[Event], event_handler: Type[EventHandler]):
        ...

    @abstractmethod
    def get_handlers(self, event: Type[Event]) -> list[Type[EventHandler]]:
        ...
