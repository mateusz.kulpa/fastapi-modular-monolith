from abc import ABC, abstractmethod
from dependency_injector import containers
from fastapi import FastAPI


class Module(ABC):
    @staticmethod
    @abstractmethod
    def register(app: FastAPI):
        ...

    @staticmethod
    @abstractmethod
    def register_dependencies(container_class: containers.DynamicContainer):
        ...
