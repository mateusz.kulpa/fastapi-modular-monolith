from abc import ABC
from contextlib import AbstractContextManager
from typing import Callable

from sqlalchemy.orm import Session


class Database(ABC):
    def session(self) -> Callable[..., AbstractContextManager[Session]]:
        ...
