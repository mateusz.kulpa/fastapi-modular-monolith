import sys
from fastapi import FastAPI
from core.infractucture.container import application_container
from modules.patient_profile import PatientProfileModule
from modules.user_profile import UserProfileModule
from modules.survey import SurveyModule
from modules.knowledge import KnowledgeModule

modules = [PatientProfileModule, UserProfileModule, SurveyModule, KnowledgeModule]

app = FastAPI()

for module in modules:
    module.register(app)
    module.register_dependencies(application_container)

application_container.wire(modules=sys.modules)

